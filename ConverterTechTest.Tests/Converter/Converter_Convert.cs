using ConverterTechTest.Components;
using ConverterTechTest.Components.Interfaces;
using ConverterTechTest.Entities;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Dynamic;

namespace ConverterTechTest.Tests
{
    public class Converter_Convert
    {
        [Test]
        public void JSONOutputProvider_WriteToFile_Normal_ReportsSuccess()
        {
            //TODO: move to common test setup

            var TestData = new ExpandoObject();
            TestData.TryAdd("Name", "Dave");
            TestData.TryAdd("Address_Line1", "Street");
            TestData.TryAdd("Address_Line2", "Town");

            var MockInputProvider = new Mock<IInputProvider>();
            MockInputProvider.Setup(x => x.ReadFromFile(It.IsAny<string>())).Returns(new List<ExpandoObject> { TestData });
            var MockOutputProvider = new Mock<IOutputProvider>();
            var MockDataProcessor = new Mock<IDataProcessor>();
            MockDataProcessor.Setup(x => x.Process(It.IsAny<ExpandoObject>())).Returns((ExpandoObject ThisValue) => ThisValue);

            var MockFactory = new Mock<IFactory>();
            MockFactory.Setup(x => x.CreateInputProvider(It.IsAny<string>())).Returns(MockInputProvider.Object);
            MockFactory.Setup(x => x.CreateOutputProvider(It.IsAny<string>())).Returns(MockOutputProvider.Object);
            MockFactory.Setup(x => x.CreateDataProcessor()).Returns(MockDataProcessor.Object);

            var MockMessaging = new Mock<IMessageProvider>();
            MockMessaging.Setup(x => x.Output(It.IsAny<string>())).Verifiable();

            var Options = new CommandLineOptions
            {
                InputPath = "Input1.xxx",
                OutputPath = "Output1.csv"
            };

            var ObjectUnderTest = new Converter(MockFactory.Object, MockMessaging.Object);
            ObjectUnderTest.Convert(Options);

            MockMessaging.Verify(x => x.Output("Successfully converted. Created Output1.csv"));
        }


    }
}