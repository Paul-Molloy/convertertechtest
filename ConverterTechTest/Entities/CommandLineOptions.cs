﻿using CommandLine;

namespace ConverterTechTest.Entities
{
    public class CommandLineOptions
    {
        [Value(index:0, Required = true, HelpText ="input file path")]
        public string InputPath { get; set; }

        [Value(index: 1, Required = true, HelpText = "output file path")]
        public string OutputPath { get; set; }
    }
}
