﻿using CommandLine;
using ConverterTechTest.Components;
using ConverterTechTest.Entities;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

[assembly: InternalsVisibleTo("ConverterTechTest.Tests")]

namespace ConverterTechTest
{
    class Program
    {
        static async Task<int> Main(string[] args)
        {
            return await Parser.Default.ParseArguments<CommandLineOptions>(args).MapResult(Main2, x => Task.FromResult(-1)); // Invalid arguments
        }

        static Task<int> Main2(CommandLineOptions options)
        {
            var ThisConverter = new Converter(new Factory(), new ConsoleMessageProvider());
            return Task.FromResult(ThisConverter.Convert(options));
        }
    }
}
