﻿using ConverterTechTest.Components.Interfaces;
using ConverterTechTest.Entities;
using System;
using System.Linq;

namespace ConverterTechTest.Components
{
    internal class Converter : IConverter
    {
        private readonly IFactory _Factory;
        private readonly IMessageProvider _Messages;
        public Converter(IFactory factory, IMessageProvider messages)
        {
            _Factory = factory;
            _Messages = messages;
        }

        public int Convert(CommandLineOptions options)
        {
            try
            {
                //TODO: confirm output file does not exist

                //TODO: use enum and get from commandline arg. Using extension for expediency
                var InputPathExtension = System.IO.Path.GetExtension(options.InputPath).ToLower();
                var InputParser = _Factory.CreateInputProvider(InputPathExtension);

                var Data = InputParser.ReadFromFile(options.InputPath);
                if (!Data.Any()) throw new Exception("No records in input");

                var ThisProcessor = _Factory.CreateDataProcessor();
                var ProcessedData = Data.Select(x => ThisProcessor.Process(x)).ToList();

                var OutputPathExtension = System.IO.Path.GetExtension(options.OutputPath).ToLower();
                var OutputWriter = _Factory.CreateOutputProvider(OutputPathExtension);
                OutputWriter.WriteToFile(ProcessedData, options.OutputPath);

                _Messages.Output($"Successfully converted. Created {options.OutputPath}");
            }
            catch (Exception ex)
            {
                //TODO: add logging system
                _Messages.Output($"Error: {ex.Message}");
                return -3;
            }
            return 0;
        }
    }
}
