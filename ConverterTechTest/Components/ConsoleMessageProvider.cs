﻿using ConverterTechTest.Components.Interfaces;
using System;

namespace ConverterTechTest.Components
{
    class ConsoleMessageProvider : IMessageProvider
    {
        public void Output(string message) => Console.WriteLine(message);
    }
}
