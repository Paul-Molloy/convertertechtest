﻿using ConverterTechTest.Components.Interfaces;
using CsvHelper;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace ConverterTechTest.Components
{
    class CSVInputProvider : IInputProvider
    {
        public List<ExpandoObject> ReadFromFile(string filePath)
        {
            //TODO: validate filepath
            try
            {
                using (var reader = new StreamReader(filePath))
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                   return csv.GetRecords<dynamic>().Cast<ExpandoObject>().ToList();
                }         
            }
            catch(Exception ex)
            {
                throw new Exception("Failed to read CSV file", ex);
            }
        }
    }
}
