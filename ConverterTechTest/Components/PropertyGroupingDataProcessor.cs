﻿using ConverterTechTest.Components.Interfaces;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;

namespace ConverterTechTest.Components
{
    internal class PropertyGroupingDataProcessor : IDataProcessor
    {
        public ExpandoObject Process(ExpandoObject input)
        {
            return GroupPropertiesByName(input);
        }

        //TODO: rewrite to be recursive if we want to handle multiple levels of _
        private ExpandoObject GroupPropertiesByName(ExpandoObject input)
        {
            var Output = new ExpandoObject();

            IDictionary<string, object> InputProperties = input;

            var PropertyNamesGroupedByPrefixes = InputProperties.Keys.GroupBy(x => x.Split("_")[0]);
            foreach (var ThisGroup in PropertyNamesGroupedByPrefixes)
            {
                if (ThisGroup.Count() == 1)
                {
                    Output.TryAdd(ThisGroup.Key, InputProperties[ThisGroup.Key]);
                }
                else
                {
                    var ThisGroupObject = new ExpandoObject();
                    foreach (var ThisPropertyName in ThisGroup)
                    {
                        var ThisPropertyWithPrefixStripped = ThisPropertyName.Substring(ThisPropertyName.IndexOf("_") + 1);
                        ThisGroupObject.TryAdd(ThisPropertyWithPrefixStripped, InputProperties[ThisPropertyName]);
                    }
                    Output.TryAdd(ThisGroup.Key, ThisGroupObject);
                }
            }

            return Output;
        }
    }
}
