﻿using ConverterTechTest.Components.Interfaces;
using System;

namespace ConverterTechTest.Components
{
    class Factory : IFactory
    {
        //TODO: implement alternative types of processing
        public IDataProcessor CreateDataProcessor() => new PropertyGroupingDataProcessor();

        public IInputProvider CreateInputProvider(string fileFormat)
        {
            return fileFormat switch
            {
                //TODO: Implement alternative classes
                // ".json" => new JSONInputProvider(),
                // ".xml" => new XMLInputProvider(),
                ".csv" => new CSVInputProvider(),
                _ => throw new Exception("Unknown input format"),
            };
        }

        public IOutputProvider CreateOutputProvider(string fileFormat)
        {
            return fileFormat switch
            {
                //TODO: Implement alternative classes
                // ".csv" => new CSVOutputProvider(),
                // ".xml" => new XMLOutputProvider(),
                ".json" => new JSONOutputProvider(),
                _ => throw new Exception("Unknown output format"),
            };
        }
    }
}
