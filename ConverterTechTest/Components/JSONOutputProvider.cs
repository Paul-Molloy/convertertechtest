﻿using ConverterTechTest.Components.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;

namespace ConverterTechTest.Components
{
    class JSONOutputProvider : IOutputProvider
    {
        public void WriteToFile(List<ExpandoObject> records, string filePath)
        {
            //TODO: validate output path, confirm no existing file
            try
            {
                var serializer = new JsonSerializer
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    Formatting = Formatting.Indented
                };

                using (var sw = new StreamWriter(filePath))
                using (var writer = new JsonTextWriter(sw))
                {
                    serializer.Serialize(writer, records);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to write JSON output", ex);
            }
        }
    }
}
