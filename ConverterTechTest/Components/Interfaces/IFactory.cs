﻿namespace ConverterTechTest.Components.Interfaces
{
    public interface IFactory
    {
        IInputProvider CreateInputProvider(string fileFormat);
        IOutputProvider CreateOutputProvider(string fileFormat);
        IDataProcessor CreateDataProcessor();
    }
}
