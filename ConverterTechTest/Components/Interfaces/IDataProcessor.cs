﻿using System.Dynamic;

namespace ConverterTechTest.Components.Interfaces
{
    public interface IDataProcessor
    {
        ExpandoObject Process(ExpandoObject input);
    }
}
