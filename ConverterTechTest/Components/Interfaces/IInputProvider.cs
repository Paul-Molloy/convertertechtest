﻿using System.Collections.Generic;
using System.Dynamic;

namespace ConverterTechTest.Components.Interfaces
{
    public interface IInputProvider
    {
        List<ExpandoObject> ReadFromFile(string filePath);

        //TODO: add ReadFromDB etc
    }
}
