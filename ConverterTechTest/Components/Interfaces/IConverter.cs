﻿using ConverterTechTest.Entities;

namespace ConverterTechTest.Components.Interfaces
{
    public interface IConverter
    {
        int Convert(CommandLineOptions options);
    }
}
