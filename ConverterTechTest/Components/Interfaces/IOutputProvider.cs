﻿using System.Collections.Generic;
using System.Dynamic;

namespace ConverterTechTest.Components.Interfaces
{
    public interface IOutputProvider
    {
        void WriteToFile(List<ExpandoObject> records, string filePath);

        //TODO: Add WriteToDB method etc
    }
}
