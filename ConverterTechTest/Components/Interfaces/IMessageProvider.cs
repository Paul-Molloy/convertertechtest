﻿namespace ConverterTechTest.Components.Interfaces
{
    public interface IMessageProvider
    {
        void Output(string message);

    }
}
